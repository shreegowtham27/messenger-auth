/*  EXPRESS SETUP  */

require("dotenv").config({ path: "./.env" });

const express = require("express");
const app = express();


// https://fbintegrator.herokuapp.com/auth/facebook/callback


app.get('/', function (req, res) {
  res.writeHead(200,{'Content-Type': 'text/html'})
  res.write(`
  <html>
  <head>
    <title>Facebook Auth</title>
  </head>
  <body>
    <a href="auth/facebook">Sign in with Facebook</a>
  </body>
</html>`
);
res.end();
})

const port = process.env.PORT || 4500;
app.listen(port, () => console.log("App listening on port " + port));

/*  PASSPORT SETUP  */

const passport = require("passport");
app.use(passport.initialize());
app.use(passport.session());

app.get("/success", (req, res) => {
  res.send("You have succssfully logged in");
});
app.get("/error", (req, res) => res.send("error logging in"));

passport.serializeUser(function(user, cb) {
  cb(null, user);
});

passport.deserializeUser(function(obj, cb) {
  cb(null, obj);
});

/*  FACEBOOK AUTH  */

const FacebookStrategy = require("passport-facebook").Strategy;

const FACEBOOK_APP_ID = process.env.FACEBOOK_APP_ID;
const FACEBOOK_APP_SECRET = process.env.FACEBOOK_APP_SECRET;

// hfgdsjsksifpoewipoqjlasjlkdfds

passport.use(
  new FacebookStrategy(
    {
      clientID:  FACEBOOK_APP_ID,
      clientSecret: FACEBOOK_APP_SECRET,
      callbackURL: `https://${process.env.DOMAIN}/auth/facebook/callback`,
      profileFields: ["id", "displayName", "photos", "email"]
    },
    function(accessToken, refreshToken, profile, cb) {
      return cb(null, profile);
    }
  )
);

app.get(
  "/auth/facebook",
  passport.authenticate("facebook", {
    scope: ["manage_pages", "pages_messaging"]
  })
);

app.get(
  "/auth/facebook/callback",
  passport.authenticate("facebook", { failureRedirect: "/error" }),
  function(req, res) {
    console.log(req.user)
    dataFormFB(req.user);
    res.redirect("/success");
  }
);

//Webhook

app.get("/webhook/", function(req, res) {
  if (req.query["hub.verify_token"] === "Thi$_!s_@_70k3n") {
    res.send(req.query["hub.challenge"]);
  }
  res.send("Error, Wrong Code");
});

/* MySQL Connections */

var mysql = require("mysql");
// var connection = mysql.createConnection({
//   host: process.env.DB_HOST ,
//   user: process.env.DB_USER ,
//   password: process.env.DB_PASSWORD ,
//   database: process.env.DB_DATABASE 
// });

// INSERT INTO `messenger` (`id`, `shop_id`, `fb_id`, `fbdisp_name`, `message`) VALUES ('4', '45454', '4545', 'djsakljdsa', '');

// INSERT INTO `messenger` (`fb_id`, `fbdisp_name`) VALUES ('454585', 'uwqioue');

// connection.connect();
function dataFormFB(user) {
  let Uid = user.id;

  let Uname = user.displayName;

  let Shop_id = 5642;

  let message = `{
    "message": "Hello World",
    "SentTo": "MessengerUser"
  }`;

  var records = [[Shop_id, Uid, Uname, message]];

  // "INSERT INTO students (name,rollno,marks) VALUES ?", [records],

//   connection.query(
//     "INSERT INTO rtl_messenger (shop_id,fb_id, fbdisp_name,message) VALUES ?",
//     [records],
//     function(err, rows, fields) {
//       if (err) throw err;
//     }
//   );
}

// Sending Message


// to post data
app.post('/webhook/', function (req, res) {
	let messaging_events = req.body.entry[0].messaging
	for (let i = 0; i < messaging_events.length; i++) {
		let event = req.body.entry[0].messaging[i]
		let sender = event.sender.id
		if (event.message && event.message.text) {
			let text = event.message.text
			if (text === 'Generic'){ 
				console.log("welcome to chatbot")
				sendGenericMessage(sender)
				continue
			}
			sendTextMessage(sender, "Text received, echo: " + text.substring(0, 200))
		}
		if (event.postback) {
			let text = JSON.stringify(event.postback)
			sendTextMessage(sender, "Postback received: "+text.substring(0, 200), token)
			continue
		}
	}
	res.sendStatus(200)
})


// recommended to inject access tokens as environmental variables, e.g.
// const token = process.env.FB_PAGE_ACCESS_TOKEN
const token = process.env.PAGE_ACCESS_TOKEN

function sendTextMessage(sender, text) {
	let messageData = { text:text }
	
	request({
		url: 'https://graph.facebook.com/v2.6/me/messages',
		qs: {access_token:token},
		method: 'POST',
		json: {
			recipient: {id:sender},
			message: messageData,
		}
	}, function(error, response, body) {
		if (error) {
			console.log('Error sending messages: ', error)
		} else if (response.body.error) {
			console.log('Error: ', response.body.error)
		}
	})
}

function sendGenericMessage(sender) {
	let messageData = {
		"attachment": {
			"type": "template",
			"payload": {
				"template_type": "generic",
				"elements": [{
					"title": "First card",
					"subtitle": "Element #1 of an hscroll",
					"image_url": "http://messengerdemo.parseapp.com/img/rift.png",
					"buttons": [{
						"type": "web_url",
						"url": "https://www.messenger.com",
						"title": "web url"
					}, {
						"type": "postback",
						"title": "Postback",
						"payload": "Payload for first element in a generic bubble",
					}],
				}, {
					"title": "Second card",
					"subtitle": "Element #2 of an hscroll",
					"image_url": "http://messengerdemo.parseapp.com/img/gearvr.png",
					"buttons": [{
						"type": "postback",
						"title": "Postback",
						"payload": "Payload for second element in a generic bubble",
					}],
				}]
			}
		}
	}
	request({
		url: 'https://graph.facebook.com/v2.6/me/messages',
		qs: {access_token:token},
		method: 'POST',
		json: {
			recipient: {id:sender},
			message: messageData,
		}
	}, function(error, response, body) {
		if (error) {
			console.log('Error sending messages: ', error)
		} else if (response.body.error) {
			console.log('Error: ', response.body.error)
		}
	})
}
